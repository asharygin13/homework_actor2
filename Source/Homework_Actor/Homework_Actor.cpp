// Copyright Epic Games, Inc. All Rights Reserved.

#include "Homework_Actor.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Homework_Actor, "Homework_Actor" );
