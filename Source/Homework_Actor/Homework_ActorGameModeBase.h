// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Homework_ActorGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_ACTOR_API AHomework_ActorGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
